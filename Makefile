# A make file that will automate the process of creating and running a Docker container


PROJECT_NAME=bmeet
# Docker tag comes from the user
DOCKER_TAG=$(shell whoami | sed 's/[^[:alnum:]._-]\+/_/g')


all:
	$(MAKE) build
	@echo "\e[5m Waiting for the application to run.. \e[25m"
	sleep 8s 
	$(MAKE) test
	$(MAKE) clean

build:
	@echo "Building the docker image.."
	docker build --network=host -t $(PROJECT_NAME):$(DOCKER_TAG) .
	@echo "Building the container.."
	docker run -itd -p 4567:4567 --name $(PROJECT_NAME) $(PROJECT_NAME):$(DOCKER_TAG)

test: #WIP - for now lets just check if we get a simple GET response.
	@echo "Testing the GET response.."
	curl localhost:4567
	@echo "\n Response: [\e[32mOK\e[0m]"	
	
clean:
	@echo "Removing the container.."
	docker ps -aq --filter name=$(PROJECT_NAME) | xargs --no-run-if-empty docker rm -f


.PHONY: all build test clean


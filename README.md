# bmeet
Simple way to find a meeting date with your friends.

Similar solutions already exist, but that's not the point.
- https://www.whenavailable.com (no signup needed)
- https://doodle.com (signup required)
- add more here if you find more examples

**The point of this project is to gather software development experience and it's ment for beginners.
The tooling used for this project should be as simple as possible.**

Develop, learn and have fun along the way!

## Intro
...

## Setup
1. Clone this repository `git clone ...`
2. Install Ruby version `2.6.5`, preferrably via [rbenv](https://github.com/rbenv/rbenv#installation)
3. Install [Bundler](https://bundler.io/) (Ruby dependency manager) via `gem install bundler`
4. Install the dependencies:
  - navigate to the project folder
  - run `bundle install`
5. Start the app: `ruby app.rb`
6. Open your browser and navigate to http://localhost:4567

### The Docker way

Prerequisites:
- Docker  (i.e debian distro: `sudo apt install docker.io`)
- Make  (i.e debian distro: `sudo apt install make`)


! If you're not familiar with **Docker** and **Makefiles** read the *Useful resources* section first !

 1. Navigate to the project folder
```sh
$ cd bmeet
```
2. Build and run the container  :
```sh
$ make build
```
3. Test the container (Note that this is intended only for testing the docker container. Later on when the app will grow, this test will not be sufficient for the application itself - so **DONT!** rely on it.):
```sh
$ make test
```
4. Open your browser and navige to http://localhost:4567
5. Remove the container:
```sh
$ make clean
```
or

2.+ 3.+ 5. Build, test and remove the container (Used to test the whole process) :
```sh
$ make all
```
 
## Useful resources

### Resources
If you've not done any software development before, start here:
- [Intro to the command line](https://launchschool.com/books/command_line)
- [Intro to Git and GitHub](https://launchschool.com/books/git) note that working with GitLab is in many ways similar to
  using GitHub
- [Intro to Ruby](https://launchschool.com/books/ruby)
- [Intro to Sinatra](http://sinatrarb.com/intro.html)
- [Intro to Docker](https://www.freecodecamp.org/news/a-beginner-friendly-introduction-to-containers-vms-and-docker-79a9e3e119b/) 
- [Intro to Makefile](https://www.gnu.org/software/make/manual/html_node/Introduction.html)


### Gems
Use the internets when needed.
These should be useful when working with the libraries:
[Sinatra homepage](http://sinatrarb.com/)
[RuboCop GH page](https://github.com/rubocop-hq/rubocop)
[Reek GH page](https://github.com/troessner/reek)
[Pry debugger GH page](https://github.com/pry/pry)
[Minitest GH page](https://github.com/seattlerb/minitest)

## Development flow
- Before you start working on a bugfix, feature, improvement, always do that on a new branch
- Branches should be named in the following format:
  - `feature/my-awesome-feature`
  - `fix/bug-that-has-to-be-fixed`
  - `update/some-update`
- Add your contribution
- Don't forget to add tests for your contribution
- Once you're happy with your work, [open a merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- Rebase your branch to latest master and ensure there are no merge conflicts
- If you open MR but still working on the code put WIP: in front of MR title
- Briefly describe your changes
- Make sure that:
  - tests pass (use minitest)
  - rubocop passes
  - reek passes
- After the merge request was approved, you can merge it

## TODO
### Gems
- [x] sinatra
- [x] pry
- [x] rubocop
- [x] reek
- [x] rspec
- [ ] orm (choose on of):
  - [ ] sequel
  - [ ] active_record
  - [ ] rom-sinatra
  - [ ] pg

### Infra stuff
- [x] dockerize
- [ ] CI/CD
- [ ] postgres

### Other
- [ ] Create mockup with [Figma](https://www.figma.com/ "figma") or other tool
- [ ] Create flow charts

### User Flow
1. Land on the main page
2. Create a new meeting
3. Choose you preferred date
4. Generate unique link
5. Generate a password that protects that link
6. Share the link with friends, so they can mark their preferred date
7. Add comment section under the calendar

FROM ruby:2.6.5

RUN apt-get update -qq && apt-get install -y build-essential

ENV APP_HOME /app
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

RUN gem install bundler:2.1.4

ADD  . $APP_HOME

RUN bundle install 

CMD ["ruby", "app.rb"]
